# Teste Madeinweb: Mobile
Desenvolver uma aplicação Mobile (YouSearch)

## Instruções
- Baixe o projeto
- Importe o mesmo utilizando o Android Studio na opção de projeto preexistente
- Ele será sincronizado com o Gradle e as dependências serão baixadas
- Execute no Android Studio de maneira tradicional, utilizando o botão Run'app'

## Especificações tecnicas
- Utilizado o padrão MVVM (ViewModel)
- API com Retrofit2
- AndroidX, ConstraintLayout e CardView
- Injeção de dependências com Kodein DI
- Estrutura dividida em ui (View), model (Modelos utilizados pela API e UI) e api (Acesso a PAI [Retrofit2])