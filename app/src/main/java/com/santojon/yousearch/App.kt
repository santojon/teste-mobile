package com.santojon.yousearch

import android.app.Application
import com.santojon.yousearch.api.Api
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

/**
 * Create App data injections and bindings
 */
class App : Application(), KodeinAware {
    // Override dependecy injection values
    override val kodein = Kodein.lazy {
        // API Singleton
        bind<Api>() with singleton { Api() }
    }
}