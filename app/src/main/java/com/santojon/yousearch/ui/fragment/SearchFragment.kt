package com.santojon.yousearch.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.santojon.yousearch.R
import com.santojon.yousearch.model.search.VideoDetails
import com.santojon.yousearch.ui.activity.BaseActivity
import com.santojon.yousearch.ui.activity.DetailsActivity
import com.santojon.yousearch.ui.activity.SearchActivity
import com.santojon.yousearch.ui.adapter.VideoContentListener
import com.santojon.yousearch.ui.adapter.VideoDetailsAdapter
import com.santojon.yousearch.ui.viewmodel.VideoViewModel
import kotlinx.android.synthetic.main.search_fragment.view.*

/**
 * Search UI
 */
class SearchFragment : BaseFragment(), VideoContentListener {
    // Recycler view related
    private lateinit var currentView: View
    private lateinit var recyclerView: RecyclerView
    private lateinit var videoDetails: MutableList<VideoDetails>

    // Screen elements
    private lateinit var searchEditText: EditText
    private lateinit var searchButton: Button

    /**
     * Called on creation
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        currentView = inflater.inflate(R.layout.search_fragment, container, false)
        recyclerView = currentView.recycle_view_video_details

        val layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager

        // get data
        videoDetails = mutableListOf()
        recyclerView.adapter = VideoDetailsAdapter(videoDetails, activity!!, this)

        // Update Videos list
        getVideos(recyclerView)

        // UI elements
        searchEditText = currentView.search_term
        searchButton = currentView.search_button

        // Add search term
        searchEditText.setText(SearchActivity.GLOBAL_SEARCH_TERM, TextView.BufferType.EDITABLE)

        // SearchButton action
        searchButton.setOnClickListener {
            // Update search term
            SearchActivity.GLOBAL_SEARCH_TERM = searchEditText.text.toString()
            getVideos(recyclerView)
            Handler().postDelayed({
                updateList()
            }, 1000)
        }

        // Update the UI
        updateUI()

        return currentView
    }

    /**
     * Update UI elements
     */
    private fun updateUI() {
        val animator = (activity as SearchActivity).animator
        animator.slideUpView(searchEditText, 1000)
        animator.slideUpView(searchButton, 1000)
        updateList()
    }

    /**
     * Update items list UI
     */
    private fun updateList() {
        val animator = (activity as SearchActivity).animator
        animator.slideUpView(recyclerView, 1500)
    }

    /**
     * Get videos from API
     */
    private fun getVideos(view: RecyclerView) {
        viewModel.search(SearchActivity.GLOBAL_SEARCH_TERM, view, activity, this)
    }

    /**
     * Do on click in an item
     */
    override fun onItemClicked(item: VideoDetails) {
        val intent = Intent(context, DetailsActivity::class.java)
        intent.putExtra(BaseActivity.VIDEO_ID, item.id.videoId)
        startActivity(intent)
    }

    /**
     * Run after creation
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(VideoViewModel::class.java).init(kodein)
    }
}
