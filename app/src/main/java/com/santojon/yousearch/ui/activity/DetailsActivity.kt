package com.santojon.yousearch.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import com.santojon.yousearch.R
import com.santojon.yousearch.ui.fragment.DetailsFragment
import kotlinx.android.synthetic.main.details_activity.*

class DetailsActivity : BaseActivity() {
    // Latouts
    private lateinit var topLayout: ConstraintLayout
    private lateinit var logoLayout: ConstraintLayout
    private lateinit var backLayout: ConstraintLayout

    /**
     * Called on creation
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Fragment calling
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.details_fragment_holder, DetailsFragment())
                .commitNow()
        }

        // Get video id from extras
        GLOBAL_VIDEO_ID = intent.extras!!.getString(VIDEO_ID)!!

        // Setup actions
        setupActions()

        // Update UI elements
        updateUI()
    }

    override fun initUI() {
        // Layout
        setContentView(R.layout.details_activity)
        topLayout = main_layout
        logoLayout = logo_container
        backLayout = back_button_container
    }

    /**
     * Setup screen elements actions
     */
    override fun setupActions() {
        // Logo click
        logoLayout.setOnClickListener {
            val intent = Intent(this, SplashScreenActivity::class.java)
            startActivity(intent)
        }

        // Back click
        backLayout.setOnClickListener {
            onBackPressed()
        }
    }

    /**
     * Update UI elements
     */
    override fun updateUI() {
        // Animate
        animator.slideUpView(topLayout, 500)
    }

    // Static
    companion object {
        // SearchTerm
        var GLOBAL_VIDEO_ID = ""
    }
}
