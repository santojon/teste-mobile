package com.santojon.yousearch.ui.viewmodel

import android.app.Activity
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.santojon.yousearch.api.PlayDetailsTask
import com.santojon.yousearch.api.SearchTask
import com.santojon.yousearch.model.detail.VideoPlay
import com.santojon.yousearch.model.search.VideoDetails
import com.santojon.yousearch.ui.adapter.VideoContentListener
import com.santojon.yousearch.ui.adapter.VideoDetailsAdapter

/**
 * ViewModel for Searching
 */
class VideoViewModel : BaseViewModel<VideoViewModel>() {
    /**
     * Search function that calls a task to get data from API and update the UI
     *
     * @param searchTerm: The search term
     * @param view: The [RecyclerView] to update the list of results
     * @param activity: The related [Activity]
     * @param listener: The [VideoContentListener] to set to the [VideoDetailsAdapter] after get data
     */
    fun search(
        searchTerm: String,
        view: RecyclerView,
        activity: Activity? = null,
        listener: VideoContentListener? = null
    ) {
        // Task to get data fro API
        SearchTask(object : SearchTask.GetApiTaskCallback {
            // Run after completion
            override fun onTaskCompleted(details: MutableList<VideoDetails>) {
                // Set the adapter
                view.adapter = VideoDetailsAdapter(
                    details,
                    activity!!,
                    listener!!
                )

                // Update UI
                activity.runOnUiThread {
                    (view.adapter as VideoDetailsAdapter).notifyDataSetChanged()
                }
            }
        }).search(api, searchTerm)  // From API with searchTerm
    }

    /**
     * Get data to views
     *
     * @param id: The video id
     * @param title: Title Textview placeholder
     * @param viewers: Viewers Textview placeholder
     * @param description: Description Textview placeholder
     */
    fun getVideoDetails(
        id: String,
        title: TextView,
        viewers: TextView,
        description: TextView
    ) {
        PlayDetailsTask(object : PlayDetailsTask.GetApiTaskCallback {
            // Run after completion
            override fun onTaskCompleted(details: MutableList<VideoPlay>) {
                // Update view data
                title.text = details[0].snippet.title
                viewers.text = details[0].statistics.viewCount
                description.text = details[0].snippet.description
            }
        }).get(api, id)
    }
}