package com.santojon.yousearch.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.lifecycle.ViewModelProviders
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.santojon.yousearch.R
import com.santojon.yousearch.ui.activity.DetailsActivity
import com.santojon.yousearch.ui.viewmodel.VideoViewModel
import kotlinx.android.synthetic.main.details_fragment.view.*

class DetailsFragment : BaseFragment() {
    // View related
    private lateinit var currentView: View
    private lateinit var video: YouTubePlayerView

    // Screen elements
    private lateinit var title: TextView
    private lateinit var viewers: TextView
    private lateinit var description: TextView

    /**
     * Called on creation
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Layout
        currentView = inflater.inflate(R.layout.details_fragment, container, false)

        // Views
        video = currentView.third_party_player_view
        title = currentView.video_title_embed
        viewers = currentView.video_viewers_embed
        description = currentView.video_description_embed

        // get video
        video.getPlayerUiController().showFullscreenButton(false)
        video.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                youTubePlayer.cueVideo(DetailsActivity.GLOBAL_VIDEO_ID, 0f)
            }
        })

        // Update Video info
        getVideo()

        return currentView
    }

    /**
     * Get videos from API
     */
    private fun getVideo() {
        viewModel.getVideoDetails(DetailsActivity.GLOBAL_VIDEO_ID, title, viewers, description)
    }

    /**
     * Run after creation
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(VideoViewModel::class.java).init(kodein)
    }
}
