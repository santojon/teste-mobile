package com.santojon.yousearch.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import com.santojon.yousearch.R
import com.santojon.yousearch.ui.fragment.SearchFragment
import kotlinx.android.synthetic.main.search_activity.*

/**
 * Activity for searching videos
 */
class SearchActivity : BaseActivity() {
    // Layouts
    private lateinit var topLayout: ConstraintLayout
    private lateinit var logoLayout: ConstraintLayout
    private lateinit var backLayout: ConstraintLayout

    /**
     * Called on creation
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Fragment calling
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.search_fragment_holder, SearchFragment())
                .commitNow()
        }

        // Get search term from extras
        GLOBAL_SEARCH_TERM = intent.extras!!.getString(SEARCH_TERM)!!

        // Setup actions
        setupActions()

        // Update UI elements
        updateUI()
    }

    override fun initUI() {
        // Layout
        setContentView(R.layout.search_activity)

        // Get view elements
        topLayout = main_layout
        logoLayout = logo_container
        backLayout = back_button_container
    }

    /**
     * Setup screen elements actions
     */
    override fun setupActions() {
        logoLayout.setOnClickListener {
            val intent = Intent(this, SplashScreenActivity::class.java)
            startActivity(intent)
        }

        // Back click
        backLayout.setOnClickListener {
            onBackPressed()
        }
    }

    /**
     * Update UI elements
     */
    override fun updateUI() {
        // Animate
        animator.slideUpView(topLayout, 500)
    }

    // Static
    companion object {
        // SearchTerm
        var GLOBAL_SEARCH_TERM = ""
    }
}
