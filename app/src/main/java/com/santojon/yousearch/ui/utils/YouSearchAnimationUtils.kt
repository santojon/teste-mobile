package com.santojon.yousearch.ui.utils

import android.content.Context
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.santojon.yousearch.R

/**
 * Util for animations
 */
class YouSearchAnimationUtils {
    /**
     * Slide a [view] UP during [duration] millis
     */
    fun slideUpView(view: View, duration: Int) {
        view.visibility = View.INVISIBLE
        slideUpView(view, duration, object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                view.visibility = View.VISIBLE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
    }

    fun slideUpView(view: View, duration: Int, animationListener: Animation.AnimationListener) {
        val slideInAnimation = AnimationUtils.loadAnimation(view.context, R.anim.slide_up)
        slideInAnimation.duration = duration.toLong()
        slideInAnimation.setAnimationListener(animationListener)
        view.startAnimation(slideInAnimation)
    }

    /**
     * Slide a [view] DOWN during [duration] millis
     */
    @JvmOverloads
    fun slideDownView(
        view: View,
        duration: Int,
        animationListener: Animation.AnimationListener = object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                view.visibility = View.GONE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        }
    ) {
        val slideDownAnimation = AnimationUtils.loadAnimation(view.context, R.anim.slide_down)
        slideDownAnimation.duration = duration.toLong()
        slideDownAnimation.setAnimationListener(animationListener)
        view.startAnimation(slideDownAnimation)
    }

    /**
     * Fade a [view] IN during [duration] millis
     */
    @JvmOverloads
    fun fadeInView(
        view: View,
        context: Context,
        duration: Int,
        animationListener: Animation.AnimationListener = object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                view.visibility = View.VISIBLE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        }
    ) {
        val fadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_in)
        fadeInAnimation.duration = duration.toLong()
        fadeInAnimation.setAnimationListener(animationListener)
        view.startAnimation(fadeInAnimation)
    }

    /**
     * Fade a [view] OUT during [duration] millis
     */
    @JvmOverloads
    fun fadeOutView(
        view: View,
        context: Context,
        duration: Int,
        animationListener: Animation.AnimationListener = object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                view.visibility = View.GONE
            }

            override fun onAnimationRepeat(animation: Animation) {}
        }
    ) {
        val fadeOutAnimation = AnimationUtils.loadAnimation(context, R.anim.fade_out)
        fadeOutAnimation.duration = duration.toLong()
        fadeOutAnimation.setAnimationListener(animationListener)
        view.startAnimation(fadeOutAnimation)
    }
}