package com.santojon.yousearch.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.santojon.yousearch.api.Api
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.generic.instance

/**
 * Base ViewModel for all needed ones
 *
 * <p>
 *     This class inject kodein defined scopes into self when extended
 * </p>
 */
abstract class BaseViewModel<out T> : ViewModel(), KodeinAware {
    // Dependency injection
    override lateinit var kodein: Kodein
    override val kodeinTrigger: KodeinTrigger = KodeinTrigger()

    // Api
    val api by instance<Api>()

    @Suppress("UNCHECKED_CAST")
    fun init(kodein: Kodein): T {
        this.kodein = kodein
        kodeinTrigger.trigger()

        return this as T
    }
}