package com.santojon.yousearch.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.santojon.yousearch.ui.viewmodel.VideoViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.android.closestKodein

/**
 * Base Fragments for all needed ones
 */
abstract class BaseFragment : Fragment(), KodeinAware {
    protected lateinit var viewModel: VideoViewModel

    // Dependency injection
    override val kodein: Kodein by closestKodein()
    override val kodeinTrigger = KodeinTrigger()

    /**
     * Do on creation
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        kodeinTrigger.trigger()

        // ViewModel provider register
        viewModel = ViewModelProviders.of(this).get(VideoViewModel::class.java).init(kodein)
    }
}