package com.santojon.yousearch.ui.adapter

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.santojon.yousearch.R
import com.santojon.yousearch.model.search.VideoDetails
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.video_detail_item.view.*

/**
 * Interface to bind the item click
 */
interface VideoContentListener {
    fun onItemClicked(item: VideoDetails)
}

/**
 * Adapter for video
 */
class VideoDetailsAdapter(
    private val videoDetailsList: MutableList<VideoDetails>,
    private val activity: Activity,
    private val listener: VideoContentListener
) : RecyclerView.Adapter<VideoDetailsAdapter.DetailsHolder>() {
    /**
     * ViewHolder for RecyclerView
     */
    class DetailsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var name: TextView
        lateinit var description: TextView
        lateinit var image: ImageView
        lateinit var details: VideoDetails

        fun bind(details: VideoDetails, listener: VideoContentListener) {
            this.details = details
            itemView.setOnClickListener {
                listener.onItemClicked(this.details)
            }
        }
    }

    /**
     * Do on create Holder (Bind layout)
     */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailsHolder {
        val myView = this.activity.layoutInflater.inflate(R.layout.video_detail_item, parent, false)
        val holder = DetailsHolder(myView)
        holder.image = myView.image_play_video
        holder.name = myView.video_name
        holder.description = myView.video_description

        return holder
    }

    /**
     * Item count
     */
    override fun getItemCount(): Int {
        return this.videoDetailsList.count()
    }

    /**
     * Do on bind data
     */
    override fun onBindViewHolder(holder: DetailsHolder, position: Int) {
        // Title
        holder.name.text = videoDetailsList[position].snippet.title

        // Description
        holder.description.text = videoDetailsList[position].snippet.description

        // Thumbnail
        Picasso.get().load(videoDetailsList[position].snippet.thumbnails["default"]?.url)
            .into(holder.image)

        // Bind it
        holder.bind(this.videoDetailsList[position], this.listener)
    }
}