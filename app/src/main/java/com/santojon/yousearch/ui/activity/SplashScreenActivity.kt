package com.santojon.yousearch.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import com.santojon.yousearch.R
import kotlinx.android.synthetic.main.activity_splash_screen.*

/**
 * Screen that represents the first contact with the app
 */
class SplashScreenActivity : BaseActivity() {
    // Layouts
    private lateinit var logoLayout: ConstraintLayout
    private lateinit var formLayout: ConstraintLayout

    // Buttons
    private lateinit var searchButton: Button

    // TextViews
    private lateinit var searchTerm: EditText

    /**
     * Run on [SplashScreenActivity] creation
     *
     * @param savedInstanceState: A [Bundle] with instance info
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupActions()
        updateUI()
    }

    /**
     * Used to init UI elemnts
     */
    override fun initUI() {
        // Set layout
        setContentView(R.layout.activity_splash_screen)

        // Get view layouts
        logoLayout = logo_container
        formLayout = form_container

        // Get buttons
        searchButton = search_button

        // Get Text views
        searchTerm = search_term
    }

    /**
     * Sets up the actions of elements interactions
     */
    override fun setupActions() {
        // Search Button
        searchButton.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            intent.putExtra(SEARCH_TERM, searchTerm.text.toString())
            startActivity(intent)
        }
    }

    /**
     * Update UI elements
     */
    override fun updateUI() {
        // Animate
        Handler().postDelayed({
            animateLogo()
        }, 1000)
    }

    /**
     * Animate Logo to get the form visible
     */
    private fun animateLogo() {
        animator.slideUpView(logoLayout, 500)
        animator.slideUpView(formLayout, 500)
    }
}
