package com.santojon.yousearch.ui.activity

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.santojon.yousearch.ui.utils.YouSearchAnimationUtils
import com.santojon.yousearch.ui.viewmodel.VideoViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.KodeinTrigger
import org.kodein.di.android.closestKodein

/**
 * Base activity for many others
 * Used to define basic properties
 */
abstract class BaseActivity : AppCompatActivity(), KodeinAware {
    // Util for animations
    lateinit var animator: YouSearchAnimationUtils

    // Dependency injection
    override val kodein: Kodein by closestKodein()
    override val kodeinTrigger = KodeinTrigger()

    // ViewModel
    protected lateinit var viewModel: VideoViewModel

    /**
     * Called on creation
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Fullscreen
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        // Get the animator
        animator = YouSearchAnimationUtils()

        // Trigger kodein DI
        kodeinTrigger.trigger()

        // ViewModel provider register
        viewModel = ViewModelProviders.of(this).get(VideoViewModel::class.java).init(kodein)

        initUI()
    }

    /**
     * Tnit UI elements
     */
    abstract fun initUI()

    /**
     * function to be overriden
     */
    abstract fun setupActions()

    /**
     * Update UI elements
     */
    abstract fun updateUI()

    // Static
    companion object {
        const val SEARCH_TERM = "SEARCH_TERM"
        const val VIDEO_ID = "VIDEO_ID"
    }
}
