package com.santojon.yousearch.model.detail

/**
 * Represents a list of properties of a video to play
 */
class VideoPlayList {
    var items = mutableListOf<VideoPlay>()
}