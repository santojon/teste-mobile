package com.santojon.yousearch.model.search

/**
 * Class that represent video details from API
 */
class VideoDetails {
    val snippet = Snippet()
    val id = Id()

    /**
     * JSON "snippet" property
     */
    class Snippet {
        val title = ""
        val description = ""

        val thumbnails = mapOf<String, Thumbnail>()

        class Thumbnail {
            val url = ""
        }
    }

    /**
     * JSON "id" property
     */
    class Id {
        val videoId = ""
    }
}