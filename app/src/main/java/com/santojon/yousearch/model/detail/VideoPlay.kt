package com.santojon.yousearch.model.detail

/**
 * Class that represent video to play details
 */
class VideoPlay {
    val snippet = Snippet()
    val id = ""
    val statistics = Statistics()

    /**
     * JSON "snippet" property
     */
    class Snippet {
        val title = ""
        val description = ""
    }

    /**
     * JSON "statistics" property
     */
    class Statistics {
        val viewCount = ""
    }
}