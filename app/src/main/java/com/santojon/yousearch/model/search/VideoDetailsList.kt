package com.santojon.yousearch.model.search

/**
 * Represents a list of Details from API
 */
class VideoDetailsList {
    var items = mutableListOf<VideoDetails>()
}