package com.santojon.yousearch.api

import com.santojon.yousearch.model.search.VideoDetails
import com.santojon.yousearch.model.search.VideoDetailsList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Task to get data from API
 */
class SearchTask(private val mCallback: GetApiTaskCallback) {
    /**
     * Search using a searchTerm from an API
     *
     * @param api: [Api] definition used to search
     * @param searchTerm: search term string
     */
    fun search(api: Api, searchTerm: String?) {
        if (searchTerm != null) {
            if (searchTerm.isNotEmpty()) {
                api.getApiService().searchVideos(
                    searchTerm,
                    Api.PART_SEARCH,
                    Api.API_KEY
                )
                    .enqueue(object : Callback<VideoDetailsList> {
                        override fun onResponse(
                            call: Call<VideoDetailsList>,
                            response: Response<VideoDetailsList>
                        ) {
                            val responseBody = response.body()
                            if (responseBody != null) {
                                mCallback.onTaskCompleted(responseBody.items)
                            }
                        }

                        override fun onFailure(call: Call<VideoDetailsList>, t: Throwable) {
                            t.printStackTrace()
                        }
                    })
            }
        }
    }

    /**
     * Callback interface to do after data acquired
     */
    interface GetApiTaskCallback {
        fun onTaskCompleted(details: MutableList<VideoDetails>)
    }
}