package com.santojon.yousearch.api

import com.santojon.yousearch.model.detail.VideoPlay
import com.santojon.yousearch.model.detail.VideoPlayList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Task to get data from API
 */
class PlayDetailsTask(private val mCallback: GetApiTaskCallback) {
    /**
     * Get video data using its id
     *
     * @param api: [Api] definition used to search
     * @param id: video id
     */
    fun get(api: Api, id: String?) {
        if (id != null) {
            if (id.isNotEmpty()) {
                api.getApiService().getVideoDetails(
                    id,
                    Api.PART_VIDEO,
                    Api.API_KEY
                )
                    .enqueue(object : Callback<VideoPlayList> {
                        override fun onResponse(
                            call: Call<VideoPlayList>,
                            response: Response<VideoPlayList>
                        ) {
                            val responseBody = response.body()
                            if (responseBody != null) {
                                mCallback.onTaskCompleted(responseBody.items)
                            }
                        }

                        override fun onFailure(call: Call<VideoPlayList>, t: Throwable) {
                            t.printStackTrace()
                        }
                    })
            }
        }
    }

    /**
     * Callback interface to do after data acquired
     */
    interface GetApiTaskCallback {
        fun onTaskCompleted(details: MutableList<VideoPlay>)
    }
}