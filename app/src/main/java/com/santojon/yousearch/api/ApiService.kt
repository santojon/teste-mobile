package com.santojon.yousearch.api

import com.santojon.yousearch.model.detail.VideoPlayList
import com.santojon.yousearch.model.search.VideoDetailsList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Define Api calls
 */
interface ApiService {
    /**
     * Search videos from API
     *
     * @param q: Query string
     * @param part: Parts to get in response
     * @param key: API key
     */
    @GET("search")
    fun searchVideos(
        @Query("q") q: String,
        @Query("part") part: String,
        @Query("key") key: String
    ): Call<VideoDetailsList>

    /**
     * Get video to play details from API
     *
     * @param id: Video id
     * @param part: Parts to get in response
     * @param key: API key
     */
    @GET("videos")
    fun getVideoDetails(
        @Query("id") id: String,
        @Query("part") part: String,
        @Query("key") key: String
    ): Call<VideoPlayList>
}