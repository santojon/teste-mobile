package com.santojon.yousearch.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Api definition
 */
class Api {
    /**
     * Get the retrofit Api builder
     */
    private fun getApiBuider(): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
    }

    /**
     * Get Api service
     */
    fun getApiService(): ApiService {
        return getApiBuider().build().create(ApiService::class.java)
    }

    // Static
    companion object {
        // Base
        const val BASE_URL = "https://www.googleapis.com/youtube/v3/"
        const val API_KEY = "AIzaSyBFFTuM5yhj0MXz9jUGyuiY0cvYNVAikIs"   // Not in the better place

        // Search related
        const val PART_SEARCH = "id,snippet"

        // Video related
        const val PART_VIDEO = "snippet,statistics"
    }
}